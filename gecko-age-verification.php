<?php
/**
 * Plugin Name: Gecko Age Verification
 * Plugin URI: https://bitbucket.org/geckodesigns/gecko-age-verification/wiki/Home
 * Description: Age Verification for WP Engine
 * Version: 1.0.0
 * Author: Gecko Designs
 * Author URI: https://geckodesigns.com
 * Requires at least: 4.4
 * Tested up to: 4.7
 *
 * Text Domain: gecko-age-verification
 * Domain Path: 
 *
 * @package GeckoAgeVerification
 * @category Core
 * @author Dwayne Parton
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Define Constants Here
define( 'GAV__FILE', __FILE__);
define( 'GAV__PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'GAV__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'GAV__LIB', plugin_dir_path( __FILE__ ).'lib/' );

// Register plugin with Gecko Command. Nothing will happen if Gecko Command is not active.
add_action('gecko/register/plugin','gecko_age_verifictaion_init');
function gecko_age_verifictaion_init(){
	new Gecko\AgeVerification\Init();
}

// If gecko comand is not installed notify the user
add_action('init','gecko_age_verifictaion_requirements');
function gecko_age_verifictaion_requirements(){
	// Make sure GeckoCommand is active
	if (!function_exists('gecko_command')){
		function gecko_age_verification_requires() {
			$class = 'notice notice-error';
			$message = __( 'Gecko Age Verification requires Gecko Command. Please contact Gecko and we will help you fix this.', 'gecko-age-verification' );
			printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
		}
		add_action( 'admin_notices', 'gecko_age_verification_requires' );
	}
}