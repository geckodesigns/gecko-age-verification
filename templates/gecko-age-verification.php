<?php 
// Template for Age verification
$minage = get_field('gecko_age_verification_minimum_age','options');
$memory = get_field('gecko_age_verification_memory','options');
$title = get_field('gecko_age_verification_title','options');
$content = get_field('gecko_age_verification_content','options');
$label = get_field('gecko_age_verification_checkbox_label','options');
$button = get_field('gecko_age_verification_button_label','options');
?>
<div id="gecko-age-verification" class="gecko-age-verification">
	<div class="gecko-age-verification__wrap">
		<div class="gecko-age-verification__title"><?php echo sprintf($title , $minage); ?></div>
		<div class="gecko-age-verification__content"><?php echo $content; ?></div>
		<form class="gecko-age-verification__form">
			<div id="gecko-age-verification-notice" class="gecko-age-verification__notice">
				Check the box to confirm your age before continuing
			</div>
			<div class="gecko-age-verification__form-line">
				<label for="gecko-age-verification-age-verified">
				<input id="gecko-age-verification-age-verified" type="checkbox" name="age-verified">
				<?php echo sprintf($label , $minage); ?>
				</label>
			</div>
			<div class="gecko-age-verification__form-line">
				<button id="gecko-age-verification-enter-site" class="gecko-age-verification__button"><?php echo $button; ?></button>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
// Set Cookie
function geckoSetCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires="+d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
// Get Cookie Value
function geckoGetCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}
// Hide Bar for a Week but if a new annoucement is made it will show
var enter_site = document.getElementById("gecko-age-verification-enter-site");
enter_site.addEventListener("click", function(event){
	event.preventDefault();
	var checkBox = document.getElementById("gecko-age-verification-age-verified");
	if (checkBox.checked == true){
		geckoSetCookie("gecko-age-verification", "Verified", <?php echo $memory; ?>);
	}else{
		document.getElementById("gecko-age-verification-notice").className += ' gecko-age-verification__notice--show';
	}
	geckoCheckCookie();
});
//Check if bar needs to be hidden
function geckoCheckCookie() {
	var cookie = geckoGetCookie("gecko-age-verification");
	if (cookie == "Verified") {
		document.getElementById("gecko-age-verification").className += ' gecko-age-verification--verified';
	}
}
geckoCheckCookie();
</script>
<style>
.gecko-age-verification {
 	position: fixed;
 	min-height: 100%;
 	min-width: 100%;
	height: 100vh;
	width: 100vw;
	background-color: #fff;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;
	z-index: 99999;
	display: flex;
	justify-content: center;
	align-items: center;
	background-color: #e4e4e4;
}
.gecko-age-verification--verified {
	display: none;
}
.gecko-age-verification__wrap {
	padding: 2.5rem 2rem;
	text-align: center;
	width:50vw;
	min-width: 280px;
	box-shadow: 0 2px 6px rgba(100, 100, 100, 0.3);
	background-color: #fff;
}
.gecko-age-verification__title {
	font-size: 2rem;
	font-weight: bold;
}
.gecko-age-verification__content {
	padding: 1rem;
}
.gecko-age-verification__button {
	transition: all 0.25s;
	padding: 1rem 2rem;
	border: 1px solid #efefef;
}
.gecko-age-verification__button:hover {
	background-color: #efefef;
}
.gecko-age-verification__form-line {
	margin-bottom: 1rem;
}
.gecko-age-verification__notice {
	display: none;
	padding: 12px;
	background-color: #ffebe8;
	border: 1px solid #c00;
	border-radius: 3px;
	margin-bottom: 0.5rem;
}
.gecko-age-verification__notice--show {
	display: block;
}
</style>