<?php
/**
 * Initiate GeckoAgeVerification.
 *
 * @author   Dwayne Parton
 * @category Class
 * @package  GeckoAgeVerification
 * @version  0.0.1
 */
namespace Gecko\AgeVerification; 

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


/**
 * Main GeckoAgeVerification Class.
 */
class Init {

	// Constructor.
	public function __construct() {
		// Create Settings Page
		self::settings_page();
		// Register Fields
		self::register_fields();
		// Add Form to Front End
		add_action('wp_footer', array( $this, 'html' ));
	}

	// Display the form
	public function html() {
		// PHP Check if it fails js will still catch it.
		// Need a way to override HTML Via theme as well.
		// Don't load on page if the cookie matches hide.
		$cookie = (isset($_COOKIE["gecko-age-verification"]))? $_COOKIE["gecko-age-verification"] : null;
		if($cookie != "Verified" && !is_user_logged_in() ):
			include_once(GAV__PLUGIN_DIR.'templates/gecko-age-verification.php');
		endif;
	}

	// Create ACF Settings Page
	// Because our plugin depends on GeckoCommand we can use all acf functions :)
	public static function settings_page() {
		if (function_exists('acf_add_options_page')) {
			//Add Settings Page
			acf_add_options_sub_page(array(
				'page_title' 	=> 'Age Verification',
				'menu_title' 	=> 'Age Verification',
				'parent_slug' 	=> 'options-general.php',
				'menu_slug' 	=> 'gecko-age-verification',
			));
		}
	}

	// Add ACF Fields
	// https://www.advancedcustomfields.com/resources/register-fields-via-php/
	// The easiest way is to use the gui and then export group via acf in php form
	public static function register_fields() {

	}

}
